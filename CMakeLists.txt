cmake_minimum_required(VERSION 3.12)
project(cours_cpp)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)
include_directories(main-atoi)
include_directories(mychainedstack)

add_executable(cours_cpp
        main-atoi/main-atoi.c
        main-atoi/myatoibase.c
        mychainedstack/main.cpp
        mychainedstack/mychainedstack.cpp
        mychainedstack/mychainedstack.h
        mychainedstack/mystack.cpp
        mychainedstack/mystack.h
        CMakeLists.txt
        Dockerfile scrabble/Main.cpp)
