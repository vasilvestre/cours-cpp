#!/usr/bin/env bash

g++ -Ofast -Wall -Werror -std=c++11 -o myfs-test myfastscrabble.cpp

BIN_LOCATION="./myfs-test"
TEST_ITER=0

# Test 1
${BIN_LOCATION} -d "./dico-test.txt" -s "estt"
status=$?
if [ $status -ne 0 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Test 2
${BIN_LOCATION} -d "/tmp/dic.txt" -s "estt" -s "abc"
status=$?
if [ $status -ne 0 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Test 3
${BIN_LOCATION} -d "/tmp/dic.txt" -s "azerazeazeazefqsef" -s "wanted"
status=$?
if [ $status -ne 0 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Test 4
${BIN_LOCATION}
status=$?
if [ $status -ne 2 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Test 5
${BIN_LOCATION} -d "/tmp/dic.txt"
status=$?
if [ $status -ne 2 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Test 6
${BIN_LOCATION} -d "/tmp/dic.txt" -saa -s "azeazrfsdaza"
status=$?
if [ $status -ne 1 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Test 7
${BIN_LOCATION} -d "/shitname/mysuperdict.txt" -saa -s "azeazrfsdaza"
status=$?
if [ $status -ne 3 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

# Tests PROF
echo "START TEST PROF"

#Test 8
${BIN_LOCATION} -d ./mysampledict.txt
status=$?
if [ $status -ne 2 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

#Test 9
${BIN_LOCATION} -d ./mysampledict.txt "plop"
status=$?
if [ $status -ne 2 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

#Test 10
${BIN_LOCATION} d ./mysampledict.txt -s -s "plop"
status=$?
if [ $status -ne 2 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

#Test 11
${BIN_LOCATION} -s "plop"
status=$?
if [ $status -ne 2 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

#Test 12
${BIN_LOCATION} -d ./mysampledict.txt -s "wzt"
status=$?
if [ $status -ne 1 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"

#Test 13
${BIN_LOCATION} -d ./mysampledict.txt -s "wzt" -s "A"
status=$?
if [ $status -ne 0 ] ; then
    exit 1
fi
((TEST_ITER++))
echo "Passed test $TEST_ITER"