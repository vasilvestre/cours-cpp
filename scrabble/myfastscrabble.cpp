//
// Created by artandor on 19/10/18.
//


#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <algorithm>

using namespace std;

int parseArgs(int argc, char **argv, vector<string> *wordsToLookFor, string *dictionnaryPath) {
    string tmp;
    for (int i = 0; i < argc; i++) {
        if (argv[i + 1]) {
            if (strcmp((char *) "-s", argv[i]) == 0) {
                tmp = argv[i + 1];
                transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
                sort(tmp.begin(), tmp.end());
                wordsToLookFor->emplace_back(tmp);
            }
            if (strcmp((char *) "-d", argv[i]) == 0) {
                *dictionnaryPath = argv[i + 1];
            }
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    string dictionaryPath, line, orderedLine;
    vector<string> wordsToLookFor, result;
    ifstream inFile;

    parseArgs(argc, argv, &wordsToLookFor, &dictionaryPath);

    if (dictionaryPath.empty() or wordsToLookFor.empty()) {
        exit(2);
    }

    inFile.open(dictionaryPath);
    if (!inFile) {
        exit(3);
    }
    while (inFile >> line) {
        orderedLine = line;
        transform(orderedLine.begin(), orderedLine.end(), orderedLine.begin(), ::tolower);
        sort(orderedLine.begin(), orderedLine.end());
        if (find(wordsToLookFor.begin(), wordsToLookFor.end(), orderedLine) != wordsToLookFor.end()) {
            result.emplace_back(line);
        }
    }

    if (result.empty()) exit(1);

    sort(result.begin(), result.end());

    for (const auto &word : result) {
        cout << word << endl;
    }

    return 0;
}
