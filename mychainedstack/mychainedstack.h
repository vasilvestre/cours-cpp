//
// Created by artandor on 05/10/18.
//

#ifndef MYCHAINEDSTACK_MYCHAINEDSTACK_H
#define MYCHAINEDSTACK_MYCHAINEDSTACK_H


#include "mystack.h"

class Mychainedstack : public Mystack {
public:
    struct container {
        int value;
        container *nextCont;
    };

    int listLen = 0;
    int resultat;

    container *list = new container[1000];

    void push(int number) override;

    int pop(void) override;

    int operator%(int mod) const override;

    void clear() override;

    virtual ~Mychainedstack();
};


#endif //MYCHAINEDSTACK_MYCHAINEDSTACK_H
